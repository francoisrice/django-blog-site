# Django Blog-Site
## CRUD application using the Django web framework

### Features
 - Displaying posts on Homepage
 - Displaying posts by a user on clicking their name
 - Post pagination
 - Resetting Users' Password
 - Giving Admin permissions to different users
 - Uploading and Changing of Profile images
 - Creating, Deleting, and Editing posts

#### Note:
 - Password recover expects a Gmail address to pass recovery emails from
 - The user and password for an email needs to be added to `settings.py` to function properly
 - Setting email to work without Gmail, can be found in the Django documentation